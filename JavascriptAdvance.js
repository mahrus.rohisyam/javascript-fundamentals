// Filter

const students = [
  { name: "Arif", finalGrade: 80 },
  { name: "Dio", finalGrade: 79.5 },
  { name: "Jodi", finalGrade: 90 },
];

//   const studentData = students.filter(data => {
//     return data.name === 'Arif'
//   })

// console.log(studentData);

//Spread Operator
const data1 = ['Student 1', 'Student 2', 'Student 3']
const data2 = ['Student 4', 'Student 5', 'Student 6']
const data3 = [...data1, ...data2, 'Student Kesekian']

const data4 = { name: "Arif", finalGrade: 80 };
const data5 = { assginment: 1 };
const dataTot = {...data1, ...data2}

// console.log(dataTot);
// console.log(data3)

const tangkap = () => {
  fetch('https://reactnative.dev/movies.json')
  .then(res => res.json())
  .then(res => console.log(res))
}

tangkap()