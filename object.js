const obj = {
  name: "Mahrus",
  zodiak: "Gemini",
  asal: "Surabaya",
  alamat: {
    propinsi: "Jawa Barat",
    kecamatan: "Sertajaya",
    zipCode: 17825,
    dusun: {
      rt: 1,
      rw: 11,
    },
  },
};

const objectArray = {
  name: "Mahrus",
  zodiak: "Gemini",
  asal: "Surabaya",
  hello: [
    1,
    2,
    3,
    "Mas Hardi",
    { nama: "Mas Novendry" },
    {
      nama: "Mas Shubhan",
      asal: "Jakarta",
      alamat: [17825, { nilai: [100, 90, 85] }],
    },
  ],
};

console.log(objectArray.hello[5].alamat[1].nilai[2]);
