//String untuk data bertipe teks
//Boolean untuk data bertipe true or false atau 0 dan 1
//Integer untuk data bertipe angka

// var a = 1       //Untuk global
// let b = 2       //Untuk suatu kelompok kode yang dapat atau tidak dapat diubah
// const c = 3     //Tidak bisa diubah

// const rani = 10 % 3

// console.log(rani)

// let b = 2       //Kelompok 1

// if(true){       //Kelompok 2
//     let b = 3
// }

// console.log(b)

// const a = 12       //Integer
// const b = 12     //String

// console.log(a == b, 'Ini dari equal') //Membandingkan nilai

// const c = 12
// const d = '12'

// console.log(c === d, 'Ini dari strict') //Untuk membandingkan tipe data

// Operasi Matika

// console.log(120 !== '120')
// console.log(12 === '12')

// var hewan = 'Kucing'
// var angka = 21

// if(hewan == 'Anjing Laut'){
//     console.log('True')
// } else {console.log('False')}
// function contoh(){

// }

// console.log(hewan.toUpperCase())

//"", ''        String Biasa
//``            Template Literal

// let coba1 = 'Ini string biasa'
// let coba2 = `ini string juga
// Support Multiline dan Embedded Expression, contoh ${coba1}
//             `
// console.log(coba2)

const x = [3, 4, 1, 6, 2, 8];
const y = [2, 7, 6, 4, 1, 1];
const a = 5;
const b = 6;

function ascending() {
  x.sort((a, b) => {
    return a - b;
  });
  console.log(x);
}

function descending() {
  y.sort((a, b) => {
    return b - a;
  });
  console.log(y);
}

function combine() {
  const sort = [...new Set([...x, ...y])];
  sort.sort((a, b) => {
    return a - b;
  });
  console.log(sort);
}

function xtoy() {
  console.log(a.toString() + b.toString());
}

function ytox() {
  console.log(b.toString() + a.toString());
}

const cek = async () => {
  let response = await fetch("https://jsonplaceholder.typicode.com/users");
  response = await response.json();
  console.log(response);
};

const ourArray = [];

for (let i = 9; i > 0; i -= 2) {
  ourArray.push(i);
}

let myArray = [1, 2, 3];
let arraySum = myArray.reduce((previous, current) => previous + current);
console.log(`Sum of array values is: ${arraySum}`);
