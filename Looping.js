// const numbers = [175, 50, 25];

// const total = ((total, num) => {
//    return console.log(total - num)
// })

const students = [
    {name: 'Arif', finalGrade: 80},
    {name: 'Dio', finalGrade: 79.5},
    {name: 'Jodi', finalGrade: 90},
  ]
  
  const totalGrade = students.reduce((preValue, currentValue) => {
    return preValue + currentValue.finalGrade
  }, 0)
  
  console.log(totalGrade)
  